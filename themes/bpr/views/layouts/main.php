<!DOCTYPE html>
<html>
<head>
<title>BPR NATASHA</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- js -->
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/jquery.min.js"></script> 
<!-- //js --> 
<link href="<?php echo Yii::app()->theme->baseUrl?>/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo Yii::app()->theme->baseUrl?>/css/timeTo.css" type="text/css" rel="stylesheet"/>
<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<head>
<link rel="icon" 
      type="image/png" 
      href="<?php echo Yii::app()->baseUrl?>/favicon.png">
</head>
<body>
	<div class="container">
		<div class="container-fluid">
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<!--<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			 <div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div> -->
			
			
			<?php echo $content ?>
		</div>
	</div>
</body>
</html>
