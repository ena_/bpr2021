<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property string $id
 * @property string $user_id
 * @property string $password
 * @property string $last_visit_date
 * @property integer $active
 * @property string $security_roles_id
 * @property string $name
 * @property string $store
 * @property integer $up
 *
 * The followings are the available model relations:
 * @property SecurityRoles $securityRoles
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, security_roles_id, store', 'required'),
			array('active, up', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>50),
			array('user_id', 'length', 'max'=>60),
			array('password, name', 'length', 'max'=>100),
			array('security_roles_id', 'length', 'max'=>36),
			array('store', 'length', 'max'=>20),
			array('last_visit_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, password, last_visit_date, active, security_roles_id, name, store, up', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'securityRoles' => array(self::BELONGS_TO, 'SecurityRoles', 'security_roles_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'password' => 'Password',
			'last_visit_date' => 'Last Visit Date',
			'active' => 'Active',
			'security_roles_id' => 'Security Roles',
			'name' => 'Name',
			'store' => 'Store',
			'up' => 'Up',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_visit_date',$this->last_visit_date,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('security_roles_id',$this->security_roles_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('up',$this->up);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
