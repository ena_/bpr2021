<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $id
 * @property integer $cif
 * @property integer $account_no
 * @property integer $months
 * @property string $fullname
 * @property integer $point
 * @property integer $status
 * @property integer $start_no
 * @property integer $finish_no
 */
class Client extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cif, account_no, months, fullname, point, status, start_no, finish_no', 'required'),
			array('cif, account_no, months, point, status, start_no, finish_no', 'numerical', 'integerOnly'=>true),
			array('fullname', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cif, account_no, months, fullname, point, status, start_no, finish_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cif' => 'Cif',
			'account_no' => 'Account No',
			'months' => 'Months',
			'fullname' => 'Fullname',
			'point' => 'Point',
			'status' => 'Status',
			'start_no' => 'Start No',
			'finish_no' => 'Finish No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cif',$this->cif);
		$criteria->compare('account_no',$this->account_no);
		$criteria->compare('months',$this->months);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('point',$this->point);
		$criteria->compare('status',$this->status);
		$criteria->compare('start_no',$this->start_no);
		$criteria->compare('finish_no',$this->finish_no);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/* public static function checkClient($no){
		$criteria = new CDbCriteria;
		$criteria->addCondition('')
		$model = Client::model()->find(array)

	}  */
}
