<?php

/**
 * This is the model class for table "history_reset".
 *
 * The followings are the available columns in table 'history_reset':
 * @property integer $id
 * @property integer $lottery_no
 * @property integer $client_id
 * @property integer $gift_id
 * @property string $reset_date
 */
class HistoryReset extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'history_reset';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lottery_no, client_id, gift_id', 'required'),
			array('lottery_no, client_id, gift_id', 'numerical', 'integerOnly'=>true),
			array('reset_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lottery_no, client_id, gift_id, reset_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lottery_no' => 'Lottery No',
			'client_id' => 'Client',
			'gift_id' => 'Gift',
			'reset_date' => 'Reset Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lottery_no',$this->lottery_no);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('gift_id',$this->gift_id);
		$criteria->compare('reset_date',$this->reset_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HistoryReset the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
