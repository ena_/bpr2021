<?php

/**
 * This is the model class for table "draw".
 *
 * The followings are the available columns in table 'draw':
 * @property integer $id
 * @property integer $lottery_no
 * @property integer $client_id
 * @property integer $gift_id
 */
class Draw extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'draw';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lottery_no, client_id, gift_id', 'required'),
			array('lottery_no, client_id, gift_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lottery_no, client_id, gift_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{	
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
			'gift' => array(self::BELONGS_TO, 'Gift', 'gift_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lottery_no' => 'Lottery No',
			'client_id' => 'Client',
			'gift_id' => 'Gift',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lottery_no',$this->lottery_no);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('gift_id',$this->gift_id);
		/* if(!isset($_GET['Presensi_sort'])){
			$criteria->order = 'gift_id DESC';
		} */
				

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50
	        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Draw the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getRandom($min,$max) {
		$x = str_split($min);
		$y = str_split($max);

		
		if($x[0] == $y[0]){
			$a = $x[0];
		}else{
			$a = rand(0,9);
		}

		if($x[1] == $y[1]){
			$b = $x[1];
		}else{
			$b = rand(0,9);
		}

		if($x[2] == $y[2]){
			$c = $x[2];
		}else{
			$c = rand(0,9);
		}
		//$d = rand(0,9);
		$d = rand($x[3],$y[3]);
		$e = rand(0,9);
		$f = rand(0,9);
		$g = rand(0,9);
		$h = rand(0,9);
		
		$arrHasil = array($a,$b,$c,$d,$e,$f,$g,$h);
		$hasil = implode('',$arrHasil);

		return $hasil;

	}
}
