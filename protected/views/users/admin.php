<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('users-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>
<div class="horz-grid">
<h3 class="head-top">Manage Users</h3>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<?php $this->widget('booster.widgets.TbButton',array(
    'label' => 'Search',
	'icon'=>'search',
	'htmlOptions'=>array('class'=>'search-button'),
)); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered condensed',
	'summaryText'=>'',
	'columns'=>array(
		array(
				'header' => '#',
				'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
			),
		//'id',
		'user_id',
		//'password',
		'last_visit_date',
		'active',
		'security_roles_id',
		
		'name',
		'store',
		'up',
		
		array(
            'header' => 'Options',
            'class'=>'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array(
                    'label' => 'lihat',
                    'options' => array(
                        'class' => 'view'
                    ),
                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))'),
                'update' => array(
                    'label' => 'ubah',
                    'options' => array(
                        'class' => 'edit'
                    ),
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))'),
                'delete' => array(
                    'label' => 'hapus',
                    'options' => array(
                        'class' => 'delete'
                    ),
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))')
            ),
            'template' => '{view}&nbsp;{update}&nbsp;{delete}',
        ),
	),
)); ?>
</div>