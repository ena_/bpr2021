<?php
/* @var $this HistoryController */
/* @var $model HistoryReset */

$this->breadcrumbs=array(
	'History Resets'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List HistoryReset', 'url'=>array('index')),
	array('label'=>'Create HistoryReset', 'url'=>array('create')),
	array('label'=>'Update HistoryReset', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete HistoryReset', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage HistoryReset', 'url'=>array('admin')),
);
?>

<h1>View HistoryReset #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'lottery_no',
		'client_id',
		'gift_id',
		'reset_date',
	),
)); ?>
