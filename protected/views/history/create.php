<?php
/* @var $this HistoryController */
/* @var $model HistoryReset */

$this->breadcrumbs=array(
	'History Resets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HistoryReset', 'url'=>array('index')),
	array('label'=>'Manage HistoryReset', 'url'=>array('admin')),
);
?>

<h1>Create HistoryReset</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>