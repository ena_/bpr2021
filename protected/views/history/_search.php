<?php
/* @var $this HistoryController */
/* @var $model HistoryReset */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lottery_no'); ?>
		<?php echo $form->textField($model,'lottery_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'client_id'); ?>
		<?php echo $form->textField($model,'client_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gift_id'); ?>
		<?php echo $form->textField($model,'gift_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reset_date'); ?>
		<?php echo $form->textField($model,'reset_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->