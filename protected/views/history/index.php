<?php
/* @var $this HistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'History Resets',
);

$this->menu=array(
	array('label'=>'Create HistoryReset', 'url'=>array('create')),
	array('label'=>'Manage HistoryReset', 'url'=>array('admin')),
);
?>

<h1>History Resets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
