<?php
/* @var $this HistoryController */
/* @var $model HistoryReset */

$this->breadcrumbs=array(
	'History Resets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List HistoryReset', 'url'=>array('index')),
	array('label'=>'Create HistoryReset', 'url'=>array('create')),
	array('label'=>'View HistoryReset', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage HistoryReset', 'url'=>array('admin')),
);
?>

<h1>Update HistoryReset <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>