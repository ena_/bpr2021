<?php
$this->breadcrumbs=array(
	'Lotteries',
);

$this->menu=array(
array('label'=>'Create Lottery','url'=>array('create')),
array('label'=>'Manage Lottery','url'=>array('admin')),
);
?>

<h1>Lotteries</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
