<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_no')); ?>:</b>
	<?php echo CHtml::encode($data->start_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('finish_no')); ?>:</b>
	<?php echo CHtml::encode($data->finish_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_id')); ?>:</b>
	<?php echo CHtml::encode($data->client_id); ?>
	<br />


</div>