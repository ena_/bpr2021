<?php
$this->breadcrumbs=array(
	'Lotteries'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Lottery','url'=>array('index')),
array('label'=>'Create Lottery','url'=>array('create')),
array('label'=>'Update Lottery','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Lottery','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Lottery','url'=>array('admin')),
);
?>

<h1>View Lottery #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'start_no',
		'finish_no',
		'client_id',
),
)); ?>
