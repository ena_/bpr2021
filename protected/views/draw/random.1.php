<?php
$cs = Yii::app()->getClientScript();
$cs->registerCss('mycss', '
    .output {
        margin: 15px;
        padding: 15px;
        background: black;
        border-radius: 10px;
        font-size: 70px;
        width: 80px;
        color: white;
        float: left;
    }
');
$url = Yii::app()->createUrl('draw/getrandom');
$url1 = Yii::app()->createUrl('draw/validate');
$url2 = Yii::app()->createUrl('draw/randomize');
$gift = $_GET['gift'];
$count = 9;
$js = <<<EOP
    $('#valid').hide();
    $('#coba').hide();
    $('#random').hide();
    

    $('#coba').click(function () {
           
        // Ajax Request
         $.ajax({
            cache:false,
            type: "GET",
            dataType: "json",
            url: "$url",
            success: function (loader) {
                //alert(loader);
                // This replace the retrieved data to the div after the setTimeOut function
                setTimeout(function () {
                    $('#nama').html('<h1><strong>' + loader.nama + '</strong></h1>');
                    $('#rek').html(loader.account);
                    $('#undi').html(loader.undian);
                }, 1000);
                pecah(loader.undian);
                //clearTimeout(t);
                generateNumber(0);
                tampil();
            }
        }); 

    });

    

    $('#start').one('click',function () {
        ambilPoin();
        //countdown();
        //$('#coba').trigger('click');
    });
    $('#stop').one('click',function () {
        cdpause();
    });

    var e = 0;
    $(window).keypress(function(event) {
        switch (event.keyCode) {  
        case 13:
            /*enter*/
             $('#start').trigger('click');
            e = 1;   
            break;
        case 32:
            /*spasi*/
            if(e == 1){
                $('#stop').trigger('click');
            }else{
                alert('anda belum pencet tombol enter !!');
            }
            break;
        case 117:
            /*u*/
             $('#ulang').trigger('click');
            break;
        case 115:
            /*S*/
             $('#sah').trigger('click');
            break; 
        case 98:
            /*b*/
            $('#back').trigger('click');
            break;        
        }
    });

    function tampil(){
        setTimeout(function(){
           $('#valid').show();
           $('#acak').hide();  
        },1000);
    };
    function pecah(loader){
        var num = loader;
        numbers=num.toString().split("").map(value=>parseInt(value,10));
        //alert(numbers);
    };
    function generateNumber(index) {
        var desired = numbers[index];
        //var duration = 1000;
        var duration = 1;

        var output = $('#output' + index); // Start ID with letter
        var started = new Date().getTime();

        animationTimer = setInterval(function() {
            if (output.text().trim() === desired || new Date().getTime() - started > duration) {
            clearInterval(animationTimer); // Stop the loop
            output.text(desired); // Print desired number in case it stopped at a different one due to duration expiration
            generateNumber(index + 1);
            } else {
            output.text(
                '' +
                Math.floor(Math.random() * 10)
            );
            }
        }, 10);
        clearTimeout(t);
    }


    $('#sah').click(function () {
        var noundi = $('div#undi').html();
        var gift = $gift;
        //alert(gift);
        //alert(noundi);
        $.ajax({
            cache:false,
            type: "GET",
            dataType: "json",
            data: {'no': noundi, 'gift':gift},
            url: "$url1",
            success: function (loader) {
                alert(loader);
                refreshPage()
            }
        }); 
        /* refreshPage() */
    }); 

    function refreshPage() {
       window.location = location.href;
    }

    $('#ulang').click(function(){
        refreshPage()
    });
    var t, count, count1;
	
	function cddisplay() {
		document.getElementById('output0').innerHTML = count;
		document.getElementById('output1').innerHTML = count1;
		document.getElementById('output2').innerHTML = count2;
		document.getElementById('output3').innerHTML = count3;
		document.getElementById('output4').innerHTML = count4;
		document.getElementById('output5').innerHTML = count5;
		document.getElementById('output6').innerHTML = count6;
		document.getElementById('output7').innerHTML = count7;
	}
	
	function countdown() {
		// starts countdown
		cddisplay();
		if (acak === 0) {
			// time is up
		} else {
			var myArrs =["0","2","4","3","1","8","6","7","5","9"];
			var myArrs1 =["0","4","2","8","7","3","6","1","5","9"];
			var myArrs2 =["8","4","2","9","1","3","5","7","6","0"];
			var myArrs3 =["9","0","6","2","7","3","4","3","5","1"];
			var myArrs4 =["7","1","3","5","4","6","0","2","8","9"];
			var myArrs5 =["4","6","5","8","7","9","0","1","3","2"];
			var myArrs6 =["2","5","4","8","7","3","6","1","0","9"];
			var myArrs7 =["1","9","4","3","5","7","2","6","5","8"];

			acak--;
			count = myArrs[urut];
			count1 = myArrs1[urut];
			count2 = myArrs2[urut];
			count3 = myArrs3[urut];
			count4 = myArrs4[urut];
			count5 = myArrs5[urut];
			count6 = myArrs6[urut];
			count7 = myArrs7[urut];
			urut++;
			if (urut == jumlah) urut = 0;
			t = setTimeout(countdown,100);
		}
	}
	
	function cdpause() {
		// pauses countdown
		//clearTimeout(t);
        //clearInterval(t);
        $('#coba').trigger('click');
    }

    function ambilPoin(){
        $.ajax({
            cache:false,
            type: "GET",
            dataType: "json",
            url: "$url2",
            success: function (loader) {
                alert(loader);
            }
        });
    }
    
	
	acak = 99999;
	urut = 0;
    count =  $count;
    count1 =  $count;
    count2 =  $count;
    count3 =  $count;
    count4 =  $count;
    count5 =  $count;
    count6 =  $count;
    count7 =  $count;
	jumlah = $count; 
	cddisplay();

      
EOP;
$cs->registerScript('random', $js, CClientScript::POS_END);
?>



<div class="row">
    <div class="col-xs-3"></div>
    <div class="col-xs-6" style="font-size:35px;" align="center"><b>Undian Tabungan Sakura</b></div>
    <div class="col-xs-3"></div>
</div>
<div class="row-fluid">
    <div class="col-xs-1"></div>
    <div class="col-xs-10">	
        <div class="output" id="output0">0</div>
        <div class="output" id="output1">0</div>
        <div class="output" id="output2">0</div>
        <div class="output" id="output3">0</div>
        <div class="output" id="output4">0</div>
        <div class="output" id="output5">0</div>
        <div class="output" id="output6">0</div>
        <div class="output" id="output7">0</div>
    </div>
    <div class="col-xs-1"></div>    
</div>
<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>
<!-- <div class="row">&nbsp;</div> -->
<div class="row-fluid">
    <div id="acak">
        <?php if($model->result != '0'){?>

            <div class="col-xs-6" align="right">
                <?php $this->widget(
                    'booster.widgets.TbButton',
                    array(
                        'id'=>'coba',
                        'label' => 'ACAK',
                        'size' => 'large',
                        'context' => 'danger',
                    )
                ); ?>

                <?php $this->widget(
                    'booster.widgets.TbButton',
                    array(
                        'id'=>'start',
                        'label' => 'start',
                        //'size' => 'large',
                        'context' => 'danger',
                        //'htmlOptions'=> array('this.onclick' => 'null'),
                    )
                ); ?>
                <?php $this->widget(
                    'booster.widgets.TbButton',
                    array(
                        'id'=>'random',
                        'label' => 'random',
                        //'size' => 'large',
                        'context' => 'danger',
                        //'htmlOptions'=> array('this.onclick' => 'null'),
                    )
                ); ?>
            </div>
            <div class="col-xs-6" align="left">
                <?php $this->widget(
                    'booster.widgets.TbButton',
                    array(
                        'id'=>'stop',
                        'label' => 'stop',
                        //'size' => 'large',
                        'context' => 'danger',
                    )
                ); ?>
            </div>
        <?php }else{ ?>
        <div class="col-xs-12" align="center">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'id'=>'back',
                    'label' => 'kembali',
                    'context' => 'success',
                    'url'=>array('/gift/choose'),
                    'buttonType'=>'link',
                )
            ); ?>
        </div>
    <?php } ?>
    </div>
</div>    
<!-- <div class="row">&nbsp;</div> -->
<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-xs-1"></div>
    <div class="col-xs-3" style="font-size:25px">Nama Lengkap</div>
    <div class="col-xs-1">:</div>
    <div class="col-xs-5" id="nama" style="font-size:25px"></div>
    <div class="col-xs-3"></div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-xs-1"></div>
    <div class="col-xs-3" style="font-size:25px">Nomor Rekening</div>
    <div class="col-xs-1">:</div>
    <div class="col-xs-3" id="rek"></div>
    <div class="col-xs-4">
        <div id="valid">
        <div class="col-xs-6" align="right">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'id'=>'sah',
                    'label' => 'SAH',
                    //'size' => 'large',
                    'context' => 'info',
                )
            ); ?>
        </div>
    <div class="col-xs-6" align="left">
        <?php $this->widget(
            'booster.widgets.TbButton',
                array(
                    'id'=>'ulang',
                    'label' => 'ulang',
                    //'size' => 'large',
                    'context'=>'primary'
                )
            ); ?>
    </div>
    </div>
    </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-xs-1"></div>
    <div class="col-xs-3" style="font-size:25px">Nomor Undian</div>
    <div class="col-xs-1">:</div>
    <div class="col-xs-4" id="undi"></div>
    <div class="col-xs-2"></div>
</div>
<div class="row"><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $model->image?>" width="250px"></img></div>
<div class="row">&nbsp;</div>
<!-- <div class="row">&nbsp;</div> -->
<?php /*
<div class="row-fluid">
    <div id="valid">
        <div class="col-xs-6" align="right">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'id'=>'sah',
                    'label' => 'SAH',
                    //'size' => 'large',
                    'context' => 'info',
                )
            ); ?>
        </div>
    <div class="col-xs-6" align="left">
        <?php $this->widget(
            'booster.widgets.TbButton',
                array(
                    'id'=>'ulang',
                    'label' => 'ulang',
                    //'size' => 'large',
                    'context'=>'primary'
                )
            ); ?>
    </div>
    </div>
</div>  
*/?>