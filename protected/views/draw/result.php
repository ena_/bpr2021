<?php
$cs = Yii::app()->getClientScript();
$js = <<<EOP
 window.onload = setupRefresh;

    function setupRefresh() {
      setTimeout("refreshPage();", 20000); // milliseconds
    }
    function refreshPage() {
       window.location = location.href;
    }
	
EOP;
$cs->registerScript('result', $js, CClientScript::POS_END);

$this->breadcrumbs=array(
	'Draws'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Draw','url'=>array('index')),
array('label'=>'Create Draw','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('draw-grid', {
data: $(this).serialize()
});
return false;
});
");
?>


<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="row">
    <div class="col-xs-3"></div>
<!--    <div class="col-xs-6" style="font-size:35px;" align="center"><b>Undian Tabungan Sakura</b></div>-->
    <div class="col-xs-3"></div>
</div>
<br><br><br><br><br><br><br><br><br>


<div class="row">
<!-- <div class="col-xs-1"></div> -->
<div class="col-xs-12" style="background: white">
	<?php $this->widget('booster.widgets.TbGroupGridView',array(
		'id'=>'draw-grid',
		'dataProvider'=>$model->search(),
		//'filter'=>$model,
		'summaryText'=>'',
		'type' => 'condensed bordered striped',
		//	'template' => "{items}",
		//'htmlOptions'=>array('class'=>'well'),
		'columns'=>array(
				array(
					'header'=>'Hadiah',
					'name'=>'gift',
					'value'=>'$data->gift_id != 0 ? $data->gift->name : ""',
				),
				array(
					'header'=>'No Undian',
					'name'=>'lottery',
					'value'=>'$data->lottery_no != 0 ? $data->lottery_no : "-"',
				),
				array(
					'header'=>'Nama Lengkap',
					'name'=>'nama',
					'value'=>'$data->client_id != 0 ? $data->client->fullname : "-"',
				),
				 array(
					'header'=>'No Rekening',
					'name'=>'member',
					'value'=>'$data->client_id != 0 ? $data->client->account_no : "-"',
				),
				//array('class'=>'booster.widgets.TbImageColumn')
				 
		),
		 'mergeColumns' => array('gift_id')
	)); ?>
    <br>

</div>
    <br><br>
    <div class="col-xs-12 pull-right" align="center">
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'id' => 'back',
                'label' => 'UNDIAN',
                'context' => 'success',
                'url' => array('/gift/choose'),
                'buttonType' => 'link',
            )
        ); ?>
    </div>
<!-- <div class="col-xs-1"></div> -->
</div>
