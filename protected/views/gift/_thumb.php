<div class="col-sm-6 col-sm-3" style="padding-top: 80px;padding-left: 40px">
<?php //echo $data->result;?>
    <div class="thumbnail">
        <?php if($data->type != 'grandprize'){?>
        <a href="<?php echo $data->result != 0 ? Yii::app()->createUrl('draw/random',array('gift'=>$data->id)) : '#'?>"><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $data->image?>" alt=""></a>
        <?php }else{ ?>
        <a href="<?php echo Yii::app()->createUrl('draw/random',array('gift'=>$data->id)) ?>"><img  src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $data->image?>" alt=""></a>
        <?php } ?>
        <div class="caption">
            <h2 align="center"><?php echo $data->name?></h2>
            <p align="center">
                <?php if($data->result != 0){
                    if($data->type != 'grandprize'){
                        $this->widget('booster.widgets.TbButton',array(
                            'id'=>'pil',
						    'label' => '',
						    'url'=>array('draw/random','gift'=>$data->id),
							'icon'=>'play-circle',
							'buttonType'=>'link',
							//'size'=>'large',
							'context'=>'info',
						));
                    }else{
                        $this->widget('booster.widgets.TbButton',array(
                            'id'=>'pilih',
						    'label' => '',
						    'url'=>array('draw/random','gift'=>$data->id),
							'icon'=>'play-circle',
							'buttonType'=>'link',
							//'size'=>'large',
							'context'=>'info',
						));     
                    }    
                   
                }else{
                     $this->widget('booster.widgets.TbButton',array(
						    'label' => '',
						    'url'=>array('draw/random'),
							'icon'=>'play-circle',
							'buttonType'=>'link',
							//'size'=>'large',
							'context'=>'info',
                            'disabled'=>true,
						));

                }?>
            </p>
        </div>
    </div>
</div>