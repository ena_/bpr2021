<?php 
$cs = Yii::app()->getClientScript();
$url = Yii::app()->createUrl('client/reset');
$js = <<<EOP
    $('#pilih').click(function () {
        $.ajax({
            cache:false,
            type: "GET",
            dataType: "json",
            url: "$url",
            success: function (loader) {
                alert(loader);
            }
        }); 
    });

EOP;
$cs->registerScript('gift', $js, CClientScript::POS_END);
?>
<!--<div class="row">-->
<!--    <div class="col-xs-3"></div>-->
<!--    <div class="col-xs-6" style="font-size:35px;" align="center"><b>Undian BPR Natasha</b></div>-->
<!--    <div class="col-xs-3"></div>-->
<!--</div>-->
<!--<div class="row">&nbsp;</div>-->
<!--<div class="row">&nbsp;</div>-->
    <div class="row-fluid" style="margin-top: 100px;margin-left: 180px;">
<?php 
    echo CHtml::openTag('div', array('class' => 'row-fluid'));
    $this->widget(
        'booster.widgets.TbThumbnails',
        array(
            'dataProvider' => $dataProvider,
            'template' => "{items}\n{pager}",
            'itemView' => '_thumb',
        )
    );
    echo CHtml::closeTag('div');
?>
</div>     