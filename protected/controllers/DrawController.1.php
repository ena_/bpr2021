<?php

class DrawController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

    /**
    * @return array action filters
    */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','create','update','admin','result','random','getrandom','validate'),
                'users'=>array('*'),
            ),
            /* array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('delete'),
                'users'=>array('admin'),
            ), */
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('reset','cleardata'),
                'users'=>array('bpr'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Draw;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Draw']))
{
$model->attributes=$_POST['Draw'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Draw']))
{
$model->attributes=$_POST['Draw'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Draw');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
    $model=new Draw('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Draw']))
    $model->attributes=$_GET['Draw'];

    $this->render('admin',array(
    'model'=>$model,
    ));
    }

    public function actionResult(){
        
        $this->layout = 'column1';

        /* $criteria = new CDbCriteria;
        $criteria->compare('gift_id','1');
        //$model = Draw::model()->findAll($criteria);
        $model = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria,
			));
        
        $criteria2 = new CDbCriteria;
        $criteria2->compare('gift_id','2');
        //$model2 = Draw::model()->findAll($criteria2);
        $model2 = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria2,
			));
        
        $criteria3 = new CDbCriteria;
        $criteria3->compare('gift_id','3');
        //$model3 = Draw::model()->findAll($criteria3);
        $model3 = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria3,
			));
        
        $criteria4 = new CDbCriteria;
        $criteria4->compare('gift_id','4');
        //$model4 = Draw::model()->findAll($criteria4);
        $model4 = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria4,
			));
        
        $criteria5 = new CDbCriteria;
        $criteria5->compare('gift_id','5');
        //$model5 = Draw::model()->findAll($criteria5);
        $model5 = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria5,
			));
        
        $criteria6 = new CDbCriteria;
        $criteria6->compare('gift_id','6');
        //$model6 = Draw::model()->findAll($criteria6);
        $model6 = new CActiveDataProvider('Draw', array(
				'criteria'=>$criteria6,
			)); */

        

         $model=new Draw('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Draw'])){
            $model->attributes=$_GET['Draw'];
        } 

        $this->render('result',array(
            'model'=>$model,
            /* 'model2'=>$model2,
            'model3'=>$model3,
            'model4'=>$model4,
            'model5'=>$model5,
            'model6'=>$model6, */
        ));
    }

    public function actionRandom($gift){
        /*get Hadiah*/
        $model = Gift::model()->findByPk($gift);

        /*begin random*/
        $dataNo = Client::model()->findAll(array(
            'condition'=>'status = 50',
        ));
        $arrData = array();
        foreach($dataNo as $val){
            $arrData[$val->id] = $val->id;
        }
        //print_r($arrData);
        //$random = array_rand($arrData,1);
        for($i=1;$i<=20;$i++){
            $random = array_rand($arrData,1);
        }
        /* echo $random;
        echo '**'; */ 
        $range = Client::model()->find(array(
            'select'=>'id, start_no, finish_no',
            'condition'=>'id = :r',
            'params' =>array(':r'=>$random),
        ));

        $min=$range->start_no;
        $max=$range->finish_no;
        
        for($i=1;$i<=20;$i++){
           $undian = rand($min,$max); 
        }
        /* echo $undian; */
        /*emd random*/
       
        //$count = '9';

        //$arrs = '["' . implode('", "', $arrUndian) . '"]';
        $this->render('random',array(
             'model'=>$model,
             //'arrs'=>$arrs,
             //'count'=>$count, 
        ));
    }

    public function actionGetRandom(){
        /* $range = Client::model()->find(array(
            'select'=>'MIN(start_no) as start_no, MAX(finish_no) as finish_no'
        ));
        $min=$range->start_no;
        $max=$range->finish_no;
        //$random = rand($min,$max);
        for($i=1;$i<=20;$i++){
           $random = rand($min,$max); 
        } */

        /*begin random*/
       /*  $dataNo = Client::model()->findAll(array(
            'condition'=>'status = 50',
        ));
        $arrData = array();
        foreach($dataNo as $val){
            $arrData[$val->id] = $val->id;
        }
        //print_r($arrData);
        //$random = array_rand($arrData,1);
        for($i=1;$i<=100;$i++){
            $random = array_rand($arrData,1);
        } */
        //$random = '5';
       /*  echo $random;
        echo '**'; */
        /* $range = Client::model()->find(array(
            'select'=>'id, start_no, finish_no',
            'condition'=>'id = :r',
            'params' =>array(':r'=>$random),
        ));

        $min=$range->start_no;
        $max=$range->finish_no;
        
        
        for($i=1;$i<=1000;$i++){
           $undian = rand($min,$max); 
        } */
        /*check dari hasil yng sudah keluar*/
       /*  $noundi = Draw::model()->find(array(
            'condition'=>'lottery_no = '.$undian.' ',
        ));
        if($noundi != null){
            for($i=1;$i<=500;$i++){
               $undian = rand($min,$max); 
            }
            //$undian = rand($min,$max);  
        } */
        /*emd random*/

        /*begin random new */
        
        $range = Client::model()->find(array(
            'select'=>'MIN(start_no) as start_no, MAX(finish_no) as finish_no',
            'condition'=>'status = 50',
        ));
        $min=$range->start_no;
        $max=$range->finish_no;

        $random = Draw::getRandom($min,$max);
		if($random<$random){
            $hasil = $random;
        }else{
            for($i=1;$i<=99999;$i++){
                $hasil = rand($min,$max); 
             }
             //print_r('dua'); 
        }
       
        /* else{
			$model = Client::model()->find(array(
				'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
				'params'=>array(':r'=>$random, ':s'=>'50')
			));
			
        } */ 
        $model = Client::model()->find(array(
            'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
            'params'=>array(':r'=>$hasil, ':s'=>'50')
        ));

        //if()
        
         /*data pemenang*/
       /*  $model = Client::model()->find(array(
            'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
            'params'=>array(':r'=>$random, ':s'=>'50')
        )); */

        //$model = Client::model()->findByPk($random);
        if($model != null){
            $temp = array("id"=>$model->id, "undian"=>$hasil, "nama"=>$model->fullname, "account"=>$model->account_no); 
        }else{
            $temp = array("id"=>'', "undian"=>$hasil, "nama"=>'-', "account"=>'-');
        }
        //$temp = array("id"=>$model->id, "undian"=>$random, "nama"=>$model->fullname, "account"=>$model->account_no);
        $data=json_encode($temp);
        echo $data;
    }

    public function actionValidate(){
        $no = $_GET['no'];
        $gift = $_GET['gift'];
        $client = Client::model()->find(array(
            'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
            'params'=>array(':r'=>$no, ':s'=>'50')
        ));
        
        $model = Draw::model()->find(array(
            'condition'=>'gift_id = :g AND lottery_no = 0 AND client_id = 0',
            'params'=>array(':g'=>$gift)
        ));
        $model->lottery_no = $no;
        $model->client_id = $client->id;
        if($model->save()){
            $giftModel = Gift::model()->findByPk($gift);
            $giftModel->result = $giftModel->result - 1;
            $giftModel->save();

            /*update status*/
           /*  $client->status = '99'; 
            $client->save(); */

            $clientModel = Client::model()->updateAll(array('status'=>'99'),'account_no ='.$client->account_no);
            $alert = 'Sukses';
        }else{
            $alert = 'error';
        }
        $data=json_encode($alert);
        echo $data;
    }

    public function actionReset(){
        
        $model = Draw::model()->count(array(
            'condition'=>'client_id = 0'
        ));

        $this->render('reset',array(
            'model'=>$model,
        ));
    }

    public function actionClearData(){
        /*reset hadiah*/
        $gift = Gift::model()->findAll();
        foreach($gift as $val){
            $model = Gift::model()->findByPk($val->id);
            $model->result = $model->total;
            $model->save();
        }

        $hasil = Draw::model()->findAll();
        foreach($hasil as $val){
            $history = new HistoryReset;
            $history->lottery_no = $val->lottery_no;
            $history->client_id = $val->client_id;
            $history->gift_id = $val->gift_id;
            $history->reset_date = date('Y-m-d H:i');
            if($history->save()){
                /*reset nasabah*/
                $cli = Client::model()->findByPk($val->client_id);
                $cli->status = '50';
                if($cli->save()){
                    /*reset draw*/
                    $dr = Draw::model()->findByPk($val->id);
                    $dr->lottery_no = '0';
                    $dr->client_id = '0';
                    $dr->save();
                }
            }
        }


    }

    public function actionGenerate(){
        $transaction=Yii::app()->db->beginTransaction();
		try
		{
			$criteria = new CDbCriteria;
			$criteria->compare('status','50');
			$criteria->select = ['id','start_no','finish_no'];
			$model = Client::model()->findAll($criteria);
			foreach($model as $val){
				for($i=$val->start_no;$i<=$val->finish_no;$i++){
					$poin = new Point;
					$poin->point = $i;
					$poin->client_id = $val->id;
					$poin->status = '1';
					$poin->save();
				}
			}
			$transaction->commit();
			$alert = 'Sukses';
		}
		catch(Exception $e)
		{
			$transaction->rollback();
			$alert = 'error';
		}
		$data=json_encode($alert);
		echo $data;
    }

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Draw::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='draw-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
