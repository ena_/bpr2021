<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		/*Yii::app()->theme = 'extjs';
		$this->layout = 'main';*/
		/*Yii::app()->theme = 'enasis';
		$this->layout = 'main';*/

		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		/* Yii::app()->theme = 'extjs';
		$this->layout = 'login'; */

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionGenerate(){
		

		$transaction=Yii::app()->db->beginTransaction();
		try
		{
		/* $connection->createCommand($sql1)->execute();
		$connection->createCommand($sql2)->execute();
		//.... other SQL executions */
			$criteria = new CDbCriteria;
			$criteria->compare('status','50');
			$criteria->select = ['id','start_no','finish_no'];
			$model = Client::model()->findAll($criteria);
			foreach($model as $val){
				for($i=$val->start_no;$i<=$val->finish_no;$i++){
					$poin = new Point;
					$poin->point = $i;
					$poin->client_id = $val->id;
					$poin->status = '1';
					$poin->save();
				}
			}
			$transaction->commit();
			$alert = 'Sukses';
		}
		catch(Exception $e)
		{
			$transaction->rollback();
			$alert = 'error';
		}
		$data=json_encode($alert);
		echo $data;
		/* $criteria = new CDbCriteria;
		$criteria->compare('status','50');
		$criteria->select = ['id','start_no','finish_no'];
		$model = Client::model()->findAll($criteria);
		foreach($model as $val){
			for($i=$val->start_no;$i<=$val->finish_no;$i++){
				$poin = new Point;
				$poin->point = $i;
				$poin->client_id = $val->id;
				$poin->status = '1';
				$poin->save();
			}
		} */
	}
	
	public function actionTest(){
		
		/* $poin2=Yii::app()->db->createCommand()
		->update(
			'point', 
			array(
				'status'=>'2',
			), 
			'client_id=:id', 
			array(':id'=>'10100081')
		);
		exit();
		$user=Yii::app()->db->createCommand()
        ->update(
            'point', 
            array(
                'status'=>'3',
            ), 
            'point=:id', 
            array(':id'=>'10043852')
        ); */
	/* 	$user = Yii::app()->db->createCommand()
			->select('id')
			->from('point')
			->where('status=:id', array(':id'=>'1'))
			->queryAll(); */
		/* print_r($user);
		exit(); *//*
		$criteria = new CDbCriteria;
		$criteria->compare('status','1');
		$model = Point::model()->findAll($criteria); */
		/* foreach($user as $val){
			echo $val['id'];
			echo '<br/>';
		}
		exit(); */
		/* $arrData = [];
        foreach($user as $val){
            $arrData[$val['id']] = $val['id'];
		}
		$x = array_rand($arrData);
		echo $x; */
		/* $a = '10026600';
		$b = '10049090';
		
		$arrTes = [];
		for($u=$a;$u<=$b;$u++){
			$arrTes[$u] = $u;
		}
		/* echo '<pre>';
		print_r($arrTes);
		echo '</pre>';
		$v = array_rand($arrTes); 
		echo $v; */
		/* for($i=1;$i<=10;$i++){
			$x = array_rand($arrTes);

			sleep(rand(0,10));
			echo $x;
			echo '<br/>';

		} */
		
		/* $arrModel = [];
		$arrPoin = [];
		$criteria = new CDbCriteria;
		$criteria->compare('status','50');
		$model = Client::model()->findAll($criteria);
		foreach($model as $val){
			for($i=$val->start_no;$i<=$val->finish_no;$i++){
				$arrPoin[] = $i;
			} */
			//$arrModel[] = $val->id;
			/* echo $val->id;
			echo '<br/>';
	/* 	}
		echo '<pre>';
		print_r($arrPoin);
		echo '</pre>'; */
		/* $no = '10033518';
		$criteria = new CDbCriteria;
		$criteria->compare('start_no','<='.$no);
		$criteria->compare('finish_no','>='.$no);
		$model = Client::model()->find($criteria); */
		/* echo $model->fullname;
		exit(); */ 

		/* $range = Client::model()->find(array(
            'select'=>'MIN(start_no) as start_no, MAX(finish_no) as finish_no',
            'condition'=>'status = 50',
        ));
        $min=$range->start_no;
		$max=$range->finish_no;
		
		for($i=1;$i<=1000;$i++){
			$undian = rand($min,$max); 
		 } */
		 /* echo $undian;
		 exit(); */
		

/*
		$x = str_split($min);
		$y = str_split($max);

		echo $min;
		echo '--';
		echo $max;
		
		if($x[0] == $y[0]){
			$a = $x[0];
		}else{
			$a = rand(0,9);
		}

		if($x[1] == $y[1]){
			$b = $x[1];
		}else{
			$b = rand(0,9);
		}

		if($x[2] == $y[2]){
			$c = $x[2];
		}else{
			$c = rand(0,9);
		}

		$d = rand($x[3],$y[3]);

		$e = rand(0,9);
		$f = rand(0,9);
		$g = rand(0,9);
		$h = rand(0,9);

		$arrHasil = array($a,$b,$c,$d,$e,$f,$g,$h);
		$hasil = implode('',$arrHasil);
		echo '==';
		echo $hasil; */


		/* $random = Draw::getRandom($min,$max);
		echo $random;
		
		if($random  > $max || $random < $min) {
			return $random;
			echo $random;	
		} 
		
		$model = Client::model()->find(array(
			'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
			'params'=>array(':r'=>$random, ':s'=>'50')
		));
		if($model != null){
			echo '**';
		echo $model->id;

		} */
		
		/* if($random > $max){
			 $random;
		}else{
			$model = Client::model()->find(array(
				'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
				'params'=>array(':r'=>$random, ':s'=>'50')
			));
			echo '**';
			echo $model->id;
		}  */

/* 
		do{
			$random;
		}
		while($random > $max);

		$model = Client::model()->find(array(
			'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
			'params'=>array(':r'=>$random, ':s'=>'50')
		));
		echo '**';
		echo $model->id;
 */
		/* $model = Client::model()->find(array(
            'condition'=>'start_no <= :r AND finish_no >= :r AND status = :s',
            'params'=>array(':r'=>'100005108', ':s'=>'50')
		));
		
		echo $model->id; */
		
		/* $a =  'adalah<br>kkk<br>bbb<br>hddhd';
		$b = explode('<br>',$a);
		//$c = implode('\r',$b);
		$c = count($b);
		for($i=0;$i<=$c-1;$i++){
			echo '<pre>';
			 echo $b[$i];
			 echo '</pre>';
		}
		
		echo '<pre>';
		print_r($c);
		echo '</pre>';

		echo Yii::app()->request->baseUrl.'/images'; */

		/*$command = $this->dbConnection->createCommand("SELECT UUID();");

		$uuid = $command->queryScalar();

		print_r($uuid);*/

		/* $min = '18000001';
		$max = '18976719';

		$x= mt_rand($min,$max);
		echo $x; */
		/* $comm = Yii::app()->db->createCommand("
		(SELECT CEIL(RAND() * MAX(id)) FROM point where STATUS=1)
		");
		$x = $comm->queryAll();
		echo ($x); */

		/* $comm2 = Yii::app()->db->createCommand("
		select * from point where id=".$x."");
		$y = $comm2->queryRow(); */

		$poin = '41322222';
		/* $model = Client::model()->find(array(
            'condition'=>'(:r between start_no and finish_no) AND status = :s',
            'params'=>array(':r'=>$poin, ':s'=>'50')
        ));
		print_r($model); */

		$command = Yii::app()->db->createCommand("select id,account_no,fullname from client WHERE ('$poin' BETWEEN start_no and finish_no) AND status=50");
		$model = $command->queryRow();
		print_r($model);
		//echo $model['account_no'].'-'. $model['fullname'];


	}
}