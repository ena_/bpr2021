<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
	array('label'=>'View <?php echo $this->modelClass; ?>','url'=>array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
);
?>

<h3>Update <?php echo $this->modelClass . " <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h3>

<?php echo "<?php"; ?> foreach(Yii::app()->user->getFlashes() as $key => $message) {
	echo '<div class="alert alert-' . $key . '">'
	 	.'<button type="button" class="close" data-dismiss="alert">×</button>'
		. $message . "</div>\n";
}
?>

<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">
					<div class="title">Ubah <?php echo $this->modelClass . " <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></div>	
				</div>
				<div class="pull-right card-action">
		            <?php echo "<?php"; ?> $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'link',
						'context'=>'primary',
						'size'=>'small',
						'label'=>'kembali',
						'icon'=>'arrow-left',
						'url'=>array('manage'),
					)); ?>
		        </div>
		        <div class="clear-both"></div>
			</div>
			<div class="card-body">
				<?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>
			</div>
		</div>
	</div>
</div>				