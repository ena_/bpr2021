<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<div class="well1 white">
	<fieldset>
	<?php echo "<?php \$form=\$this->beginWidget('booster.widgets.TbActiveForm',array(
		'id'=>'" . $this->class2id($this->modelClass) . "-form',
		'enableAjaxValidation'=>false,
	)); ?>\n"; ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> wajib diisi.</p>

	<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

	<?php
	foreach ($this->tableSchema->columns as $column) {
		if ($column->autoIncrement) {
			continue;
		}
		?>
		<?php echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>

	<?php
	}
	?>
	<div class="form-group">
		<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'size'=>'small',
				'label'=>\$model->isNewRecord ? 'Tambah' : 'Simpan',
			)); ?>\n"; ?>
		<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'reset',
				'context'=>'primary',
				'size'=>'small',
				'label'=>'Reset',
			)); ?>\n"; ?>	
	</div>

	<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
	</fieldset>

</div>
