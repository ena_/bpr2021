<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
	return false;
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<?php echo "<?php"; ?> foreach(Yii::app()->user->getFlashes() as $key => $message) {
	echo '<div class="alert alert-' . $key . '">'
	 	.'<button type="button" class="close" data-dismiss="alert">×</button>'
		. $message . "</div>\n";
}
?>

<div class="row">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-header">
				<div class="card-title">
					<div class="title">Kelola <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></div>	
				</div>
				<div class="pull-right card-action">
		            <?php echo "<?php"; ?> $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'link',
						'context'=>'primary',
						'size'=>'small',
						'label'=>'cari',
						'icon'=>'search',
						'htmlOptions'=>array('class'=>'search-button'),
					)); ?>
		            <?php echo "<?php"; ?> $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'link',
						'context'=>'primary',
						'size'=>'small',
						'label'=>'tambah',
						'icon'=>'plus',
						'url'=>array('create'),
					)); ?>
		        </div>
		        <div class="clear-both"></div>
			</div>
			<div class="card-body">
				<div class="search-form" style="display:none">
					<?php echo "<?php \$this->renderPartial('_search',array(
						'model'=>\$model,
					)); ?>\n"; ?>
				</div><!-- search-form -->	
				<?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView',array(
					'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'type'=>'hover striped bordered',	
					'summaryText'=>'',
					'columns'=>array(
						array(
							'header' => '#',
							'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
						),
						<?php
						$count = 0;
						foreach ($this->tableSchema->columns as $column) {
							if (++$count == 7) {
								echo "\t\t/*\n";
							}
							echo "\t\t'" . $column->name . "',\n";
						}
						if ($count >= 7) {
							echo "\t\t*/\n";
						}
						?>
						array(
				            'header' => 'Options',
				            'class'=>'booster.widgets.TbButtonColumn',
				            'buttons' => array(
				                'view' => array(
				                    'label' => 'lihat',
				                    'options' => array(
				                        'class' => 'view'
				                    ),
				                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))'),
				                'update' => array(
				                    'label' => 'ubah',
				                    'options' => array(
				                        'class' => 'edit'
				                    ),
				                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))'),
				                'delete' => array(
				                    'label' => 'hapus',
				                    'options' => array(
				                        'class' => 'delete'
				                    ),
				                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))')
				            ),
				            'template' => '{view}&nbsp;{update}&nbsp;{delete}',
				        ),
					),
				)); ?>
			</div>
		</div>	
	</div>
</div>
